General Information
===================

gtk-update-icon-cache is a tool from GTK+ to update icons and themes caches.

It is split from the GTK+ sources by the Gentoo Gnome team to help GTK+ 2 and 3
to co-exist better by extracting the single conflicting tool.

Official sources of this tool are located at:

https://gitlab.gnome.org/GNOME/gtk/blob/gtk-3-24/gtk/updateiconcache.c

Installation
============

gtk-update-icon-cache requires GLib >= 2.53.4 and gdk-pixbuf >= 2.30.0.

Updating
========

* Run `utils/update.sh <release tag>` in toplevel source directory.
* Update version number in `meson.build`
* Review the updates it did.
  - Watch out for C code changes needing something extra from the build system
    (e.g. more preprocessor macros to declare).
* Synchronize dependencies to glib and gdk-pixbuf, even if not strictly needed.
* Commit the updates and tag it.
* Push it (don't forget to push tags)

From there
`https://gitlab.gnome.org/Community/gentoo/gtk-update-icon-cache/-/archive/<version>/gtk-update-icon-cache-<version>.tar.bz2`
can be used as source tarball.

How to report bugs
==================

Bugs should be reported to the Gentoo bug tracking system
(http://bugs.gentoo.org, product Gentoo Linux, component Current packages).
You will need to create an account for yourself.

In the bug report, please include:

* Information about your system. For instance:

   - What version of gtk-update-icon-cache
   - What operating system and version
   - What version of glib and gdk-pixbuf libraries

 And anything else you think is relevant.

* How to reproduce the bug.

* If the bug was crash, the exact text that was printed out when the
  crash occured.

* Further information such as stack traces may be useful, but is not
  necessary.

Please check the bugzilla pages for the list of known bugs.

Alternatively you can post issues to [GNOME GitLab project](https://gitlab.gnome.org/Community/gentoo/gtk-update-icon-cache),
but you may need to notify the Gentoo GNOME team off-band about it.

Patches
=======

Patches should also be submitted to bugs.gentoo.org. If the patch
fixes an existing bug, add the patch as an attachment to that bug
report.

Otherwise, enter a new bug report that describes the problem the patch
fixes, and attach it to that bug report.

Patches should be created with the git format-patch command.

Alternatively you can submit merge requests to the [GNOME GitLab project](https://gitlab.gnome.org/Community/gentoo/gtk-update-icon-cache),
but you may need to notify the Gentoo GNOME team off-band about it.



The Gentoo Gnome team.
