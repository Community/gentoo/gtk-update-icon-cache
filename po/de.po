# German GTK+ translation
# Copyright (C) 1998-2004 Free Software Foundation, Inc.
#
# IM = Input method => Eingabemethode
#
# #. * Translators, the strings in the 'keyboard label' context are
# #. * display names for keyboard keys. Some of them have prefixes like
# #. * XF86 or ISO_ - these should be removed in the translation. Similarly,
# #. * underscores should be replaced by spaces. The prefix 'KP_' stands
# #. * for 'key pad' and you may want to include that in your translation.
# #. * Here are some examples of English translations:
# #. * XF86AudioMute - Audio mute
# #. * Scroll_lock   - Scroll lock
# #. * KP_Space      - Space (keypad)
#
#
# Daniel Egger <Daniel.Egger@t-online>, 1998.
# Karsten Weiss <karsten@addx.au.s.shuttle.de>, 1999.
# Matthias Warkus <mawarkus@gnome.org>, 2001, 2002.
# Hendrik Brandt <heb@gnome-de.org>, 2004-2005.
# Christian Neumair <chris@gnome-de.org>, 2002-2004.
# Andre Klapper <ak-47@gmx.net>, 2008.
# Hendrik Richter <hendrikr@gnome.org>, 2004-2009.
# Jakob Kramer <jakob.kramer@gmx.de>, 2010.
# Paul Seyfert <pseyfert@mathphys.fsk.uni-heidelberg.de>, 2011.
# Christian Kirbach <christian.kirbach@gmail.com>, 2009-2011, 2012, 2021.
# Bernd Homuth <dev@hmt.im>, 2015.
# Wolfgang Stöggl <c72578@yahoo.de>, 2014, 2016.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2009-2013, 2016-2018, 2020.
# Tim Sabsch <tim@sabsch.com>, 2018-2020.
# Philipp Kiemle <philipp.kiemle@gmail.com>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: GTK+ master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-24 16:52+0000\n"
"PO-Revision-Date: 2023-03-24 23:46+0100\n"
"Last-Translator: Christian Kirbach <christian.kirbach@gmail.com>\n"
"Language-Team: Deutsch <gnome-de@gnome.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: gtk/updateiconcache.c:1396
#, c-format
msgid "Failed to write header\n"
msgstr "Header konnte nicht geschrieben werden\n"

#: gtk/updateiconcache.c:1402
#, c-format
msgid "Failed to write hash table\n"
msgstr "Hash-Tabelle konnte nicht geschrieben werden\n"

#: gtk/updateiconcache.c:1408
#, c-format
msgid "Failed to write folder index\n"
msgstr "Ordner-Index konnte nicht geschrieben werden\n"

#: gtk/updateiconcache.c:1416
#, c-format
msgid "Failed to rewrite header\n"
msgstr "Header konnte nicht überschrieben werden\n"

#: gtk/updateiconcache.c:1510
#, c-format
msgid "Failed to open file %s : %s\n"
msgstr "Datei %s konnte nicht geöffnet werden: %s\n"

#: gtk/updateiconcache.c:1518 gtk/updateiconcache.c:1548
#, c-format
msgid "Failed to write cache file: %s\n"
msgstr "Cache-Datei konnte nicht gespeichert werden: %s\n"

#: gtk/updateiconcache.c:1558
#, c-format
msgid "The generated cache was invalid.\n"
msgstr "Der erstellte Cache war ungültig.\n"

#: gtk/updateiconcache.c:1572
#, c-format
msgid "Could not rename %s to %s: %s, removing %s then.\n"
msgstr ""
"»%s« konnte nicht in »%s« umbenannt werden: %s; »%s« wird deswegen "
"entfernt.\n"

#: gtk/updateiconcache.c:1586
#, c-format
msgid "Could not rename %s to %s: %s\n"
msgstr "»%s« konnte nicht in »%s« umbenannt werden: %s\n"

#: gtk/updateiconcache.c:1596
#, c-format
msgid "Could not rename %s back to %s: %s.\n"
msgstr "»%s« konnte nicht zurück in »%s« umbenannt werden: %s.\n"

#: gtk/updateiconcache.c:1623
#, c-format
msgid "Cache file created successfully.\n"
msgstr "Cache-Datei wurde erfolgreich erstellt.\n"

#: gtk/updateiconcache.c:1662
msgid "Overwrite an existing cache, even if up to date"
msgstr "Existierenden Cache überschreiben, auch wenn dieser aktuell ist"

#: gtk/updateiconcache.c:1663
msgid "Don't check for the existence of index.theme"
msgstr "Nicht auf Existenz von index.theme überprüfen"

#: gtk/updateiconcache.c:1664
msgid "Don't include image data in the cache"
msgstr "Bilddaten nicht in Cache einbinden"

#: gtk/updateiconcache.c:1665
msgid "Include image data in the cache"
msgstr "Bilddaten in Zwischenspeicher einbinden"

#: gtk/updateiconcache.c:1666
msgid "Output a C header file"
msgstr "C-Header-Datei ausgeben"

#: gtk/updateiconcache.c:1667
msgid "Turn off verbose output"
msgstr "Detaillierte Ausgabe deaktivieren"

#: gtk/updateiconcache.c:1668
msgid "Validate existing icon cache"
msgstr "Symbol-Cache wird überprüft"

#: gtk/updateiconcache.c:1735
#, c-format
msgid "File not found: %s\n"
msgstr "Datei nicht gefunden: %s\n"

#: gtk/updateiconcache.c:1741
#, c-format
msgid "Not a valid icon cache: %s\n"
msgstr "Kein gültiger Symbol-Cache: %s\n"

#: gtk/updateiconcache.c:1754
#, c-format
msgid "No theme index file.\n"
msgstr "Keine Thema-Indexdatei.\n"

#: gtk/updateiconcache.c:1758
#, c-format
msgid ""
"No theme index file in '%s'.\n"
"If you really want to create an icon cache here, use --ignore-theme-index.\n"
msgstr ""
"Keine Themenindex-Datei in »%s«.\n"
"Wenn Sie hier wirklich einen Symbol-Cache erstellen möchten, verwenden Sie "
"bitte --ignore-theme-index.\n"

