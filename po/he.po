# translation of gtk+.HEAD.he.po to Hebrew
# Hebrew Translation for GTK+ - תרגום עברי ל-
# Copyright (C) 2000,2002, 2004 Free Software Foundation, Inc.
# Tzafrir Cohen <tzafrir@technion.ac.il>, 2000.
# Gil 'Dolfin' Osher <dolfin@rpg.org.il>, 2002.
# Gil Osher <dolfin@rpg.org.il>, 2004.
# Yair Hershkovitz <yairhr@gmail.com>, 2006.
# Yaron Shahrabani <sh.yaron@gmail.com>, 2011, 2012.
# Yosef Or Boczko <yoseforb@gmail.com>, 2013-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: gtk+.HEAD.he\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-04-04 09:09+0000\n"
"PO-Revision-Date: 2024-04-07 08:12+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <yoseforb@gmail.com>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==2 ? 1 : n>10 && n%10==0 ? 2 : "
"3);\n"
"X-Generator: Poedit 3.4.2\n"
"X-Project-Style: default\n"

#: gtk/updateiconcache.c:1396
#, c-format
msgid "Failed to write header\n"
msgstr "Failed to write header\n"

#: gtk/updateiconcache.c:1402
#, c-format
msgid "Failed to write hash table\n"
msgstr "Failed to write hash table\n"

#: gtk/updateiconcache.c:1408
#, c-format
msgid "Failed to write folder index\n"
msgstr "Failed to write folder index\n"

#: gtk/updateiconcache.c:1416
#, c-format
msgid "Failed to rewrite header\n"
msgstr "Failed to rewrite header\n"

#: gtk/updateiconcache.c:1510
#, c-format
msgid "Failed to open file %s : %s\n"
msgstr "Failed to open file %s : %s\n"

#: gtk/updateiconcache.c:1518 gtk/updateiconcache.c:1548
#, c-format
msgid "Failed to write cache file: %s\n"
msgstr "Failed to write cache file: %s\n"

#: gtk/updateiconcache.c:1558
#, c-format
msgid "The generated cache was invalid.\n"
msgstr "The generated cache was invalid.\n"

#: gtk/updateiconcache.c:1572
#, c-format
msgid "Could not rename %s to %s: %s, removing %s then.\n"
msgstr "Could not rename %s to %s: %s, removing %s then.\n"

#: gtk/updateiconcache.c:1586
#, c-format
msgid "Could not rename %s to %s: %s\n"
msgstr "Could not rename %s to %s: %s\n"

#: gtk/updateiconcache.c:1596
#, c-format
msgid "Could not rename %s back to %s: %s.\n"
msgstr "Could not rename %s back to %s: %s.\n"

#: gtk/updateiconcache.c:1623
#, c-format
msgid "Cache file created successfully.\n"
msgstr "Cache file created successfully.\n"

#: gtk/updateiconcache.c:1662
msgid "Overwrite an existing cache, even if up to date"
msgstr "Overwrite an existing cache, even if up to date"

#: gtk/updateiconcache.c:1663
msgid "Don't check for the existence of index.theme"
msgstr "לא לבדוק אם יש index.theme"

#: gtk/updateiconcache.c:1664
msgid "Don't include image data in the cache"
msgstr "לא לכלול את נתוני התמונה במטמון"

#: gtk/updateiconcache.c:1665
msgid "Include image data in the cache"
msgstr "Include image data in the cache"

#: gtk/updateiconcache.c:1666
msgid "Output a C header file"
msgstr "Output a C header file"

#: gtk/updateiconcache.c:1667
msgid "Turn off verbose output"
msgstr "Turn off verbose output"

#: gtk/updateiconcache.c:1668
msgid "Validate existing icon cache"
msgstr "Validate existing icon cache"

#: gtk/updateiconcache.c:1735
#, c-format
msgid "File not found: %s\n"
msgstr "קובץ לא נמצא: %s\n"

#: gtk/updateiconcache.c:1741
#, c-format
msgid "Not a valid icon cache: %s\n"
msgstr "Not a valid icon cache: %s\n"

#: gtk/updateiconcache.c:1754
#, c-format
msgid "No theme index file.\n"
msgstr "No theme index file.\n"

#: gtk/updateiconcache.c:1758
#, c-format
msgid ""
"No theme index file in '%s'.\n"
"If you really want to create an icon cache here, use --ignore-theme-index.\n"
msgstr ""
"אין קובץ מפתח של ערכת עיצוב ב־‚%s’.\n"
"אם באמת צריך ליצור כאן מטמון סמלים, יש להשתמש ב־‎--ignore-theme-index.\n"

