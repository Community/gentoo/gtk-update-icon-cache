# kikongo translation of GTK+ 2.x.
# Copyright (C) 1998-2010 Free Software Foundation, Inc.
# This file is distributed under the GNU Library General Public License Version 2.
#
# Kibavuidi Nsiangani <lundombe01@zaya-dio.com>, 2006-2010
#
msgid ""
msgstr ""
"Project-Id-Version: gtk+ HEAD\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gtk%2b&component=general\n"
"POT-Creation-Date: 2010-09-22 11:19+0000\n"
"PO-Revision-Date: 2010-10-04 16:32+0100\n"
"Last-Translator: Kibavuidi Nsiangani <lundombe01@zaya-dio.com>\n"
"Language-Team: GNOME kikongo Team <lundombe01@zaya-dio.com>\n"
"Language: kg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n>1;\n"

#: ../gtk/updateiconcache.c:492
#: ../gtk/updateiconcache.c:552
#, c-format
msgid "different idatas found for symlinked '%s' and '%s'\n"
msgstr "bifiuma bimonekene muna « %s » ye « %s » \n"

#: ../gtk/updateiconcache.c:1374
#, c-format
msgid "Failed to write header\n"
msgstr "mpu bu yivangama nkatu\n"

#: ../gtk/updateiconcache.c:1380
#, c-format
msgid "Failed to write hash table\n"
msgstr "kisekokilu bu yi soba kio nkatu\n"

#: ../gtk/updateiconcache.c:1386
#, c-format
msgid "Failed to write folder index\n"
msgstr "ntaangulu a nta yi, bu yisonama nkatu\n"

#: ../gtk/updateiconcache.c:1394
#, c-format
msgid "Failed to rewrite header\n"
msgstr "mpu bu isobua nkatu\n"

#: ../gtk/updateiconcache.c:1463
#, c-format
msgid "Failed to open file %s : %s\n"
msgstr "« %s » bu dizibuka nkatu : %s\n"

#: ../gtk/updateiconcache.c:1471
#, c-format
msgid "Failed to write cache file: %s\n"
msgstr "kinkoso bu kivangama nkatu : %s\n"

#: ../gtk/updateiconcache.c:1507
#, c-format
msgid "The generated cache was invalid.\n"
msgstr "kikoso ki uvangidi, kena ya betila ko.\n"

#: ../gtk/updateiconcache.c:1521
#, c-format
msgid "Could not rename %s to %s: %s, removing %s then.\n"
msgstr "%s bu kibaka nkumbu andi %s nkatu : %s, ibobo ngieti katula %s.\n"

#: ../gtk/updateiconcache.c:1535
#, c-format
msgid "Could not rename %s to %s: %s\n"
msgstr "%s bu kibaka nkumbu andi %s nkatu: %s\n"

#: ../gtk/updateiconcache.c:1545
#, c-format
msgid "Could not rename %s back to %s: %s.\n"
msgstr "%s bu kivutukila nkumbu andi %s nkatu: %s\n"

#: ../gtk/updateiconcache.c:1572
#, c-format
msgid "Cache file created successfully.\n"
msgstr "kinoso kitomene vangua.\n"

#: ../gtk/updateiconcache.c:1611
msgid "Overwrite an existing cache, even if up to date"
msgstr "yiingasa nkoso kaka"

#: ../gtk/updateiconcache.c:1612
msgid "Don't check for the existence of index.theme"
msgstr "index.theme, kusolodi yo ko"

#: ../gtk/updateiconcache.c:1613
msgid "Don't include image data in the cache"
msgstr "mafiuma ma bifuanisu, veengula mo mu nkoso"

#: ../gtk/updateiconcache.c:1614
msgid "Output a C header file"
msgstr "vayikisae mpukilala ya C"

#: ../gtk/updateiconcache.c:1615
msgid "Turn off verbose output"
msgstr "katulae mafiuma ma bizayikusu"

#: ../gtk/updateiconcache.c:1616
msgid "Validate existing icon cache"
msgstr "ntambudi nkoso a biteke"

#: ../gtk/updateiconcache.c:1683
#, c-format
msgid "File not found: %s\n"
msgstr "kilala %s kimonekene ko\n"

#: ../gtk/updateiconcache.c:1689
#, c-format
msgid "Not a valid icon cache: %s\n"
msgstr "ka yi kisasilu kia mvuaatu kia mbote ko : %s\n"

#: ../gtk/updateiconcache.c:1702
#, c-format
msgid "No theme index file.\n"
msgstr "kisasilu kia mvuaatu nkatu.\n"

#: ../gtk/updateiconcache.c:1706
#, c-format
msgid ""
"No theme index file in '%s'.\n"
"If you really want to create an icon cache here, use --ignore-theme-index.\n"
msgstr ""
"kisasilu kia mvuaatu nkatu ku « %s ».\n"
"nkatika vo zolele vaangae nkoso va fulu ki, sadilae --ignore-theme-index.\n"

