#!/bin/bash

URL="https://gitlab.gnome.org/GNOME/gtk/raw/${1}/"
FILES=(
	gtk/gtkiconcachevalidator.c
	gtk/gtkiconcachevalidator.h
	gtk/updateiconcache.c
	po/LINGUAS
)
DOC_FILE=(
	docs/reference/gtk/gtk-update-icon-cache.xml docs/gtk-update-icon-cache.xml
)
error=0

# Download straight updates
for f in ${FILES[@]}; do
	echo "GET: $URL$f"
	if $(curl -sf $URL$f > $f); then
		echo " OK: $f"
	else
		echo "ERR: $f download error"
		error=1
	fi
done

# Update docs (different path)
f=${DOC_FILE[0]}
echo "GET: $URL$f"
if curl -sf $URL$f > ${DOC_FILE[1]}; then
	echo " OK: $f"
else
	echo "ERR: $f download error"
	error=1
fi

rm -v po/*.po
while IFS= read -r lang
do
	f="po/${lang}.po"
	echo "GET: $URL$f"
	if curl -sf $URL$f > $f.tmp; then
		utils/extract-po.py ${f}.tmp ${f}
		rm ${f}.tmp
		echo " OK: $f"
	else
		echo "ERR: $f download error"
		error=1
	fi
done <<< $(grep -v '^#' po/LINGUAS | grep -v '^$')

if [ $error -eq 0 ]; then
	echo "SUCCESS: All updates completed successfully."
else
	echo "WARNING: One or more updates encountered an error!"
fi

# Once we hook it up as a meson run_target:
#exit $error
